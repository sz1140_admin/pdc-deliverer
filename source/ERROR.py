#author#======================================
#file:ERROR.py
#wangxiaochen CFR7445 2022.01.14
#Copyright (c) 2022 一天两根冰棍
#=============================================

#key value define#============================
#定义错误代码------
error_number_not_found              = "001"
error_information_code_not_found    = "002"
error_airport_not_found             = "003"
error_string_length_not_match       = "004"
error_language_not_support          = "005"
error_runway_mode_not_found         = "006"
#=============================================

#function#====================================
def error_handle(error_code):
    pass
#=============================================
